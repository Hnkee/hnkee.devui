﻿using System;
using System.Collections.Generic;

namespace Henke.DevUI.Policy.Common
{
    /// <summary>
    /// Represents an abstraction for Assembly
    /// </summary>
    public interface IAssembly
    {
        /// <summary>
        /// Gets all types in the assembly (ies)
        /// </summary>
        /// <returns>AllTypes</returns>
        IList<Type>  AllTypes();
    }
}