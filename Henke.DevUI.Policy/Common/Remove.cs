﻿namespace Henke.DevUI.Policy.Common
{
    public static class Remove
    {
        public static string LastDotBefore(string parameter, string target)
        {
            var indexOfParameter = target.IndexOf(parameter);
            if (indexOfParameter == -1) return target;
            var removed = target.Remove(indexOfParameter);
            var lastDot = removed.LastIndexOf(".");
            if (lastDot != -1)
            {
                return target.Remove(lastDot);
            }
            return removed;
        }
    }
}