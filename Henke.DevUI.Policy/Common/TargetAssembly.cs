﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Henke.DevUI.Policy.Common
{
    /// <summary>
    /// Simple implementation of IAssembly
    /// </summary>
    /// <remarks>
    /// Loads only once (on construct)
    /// Build your own implementation (might load several assemblies, load in the background,lazyloading etc)
    /// </remarks>
    internal class TargetAssembly : IAssembly
    {
        private readonly IList<Type> _allTypesInAssembly;

        internal TargetAssembly(Assembly assembly)
        {
            _allTypesInAssembly = assembly.GetTypes().ToList();
        }

        public IList<Type> AllTypes()
        {
            return _allTypesInAssembly;
        }

    }
}