﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Henke.DevUI.Policy.Common
{
    public class PolicyTarget : IPolicyTarget
    {
        public Type TargetType { get; private set; }

        public string Name
        {
            get { return TargetType.Name; }
        }

        public string FolderName
        {
            get { return TargetType.Namespace != null ? TargetType.Namespace.Split('.').Last() : string.Empty; }
        }

        public string NameSpace
        {
            get { return TargetType.Namespace; }
        }

        public IEnumerable<Type> AllExposedTypes
        {
            get
            {
                var publicProps = from p in TargetType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                                  select p.PropertyType;

                var publicFields = from p in TargetType.GetFields(BindingFlags.Public | BindingFlags.Instance)
                                    select p.FieldType;

                return publicProps.Concat(publicFields).Distinct();
            }
        }

        public PolicyTarget(Type targetType)
        {
            TargetType = targetType;
        }
    }
}