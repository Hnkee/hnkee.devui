﻿using System.Collections.Generic;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.Common
{
    /// <summary>
    /// A Repository for getting all the PolicyTargets
    /// </summary>
    public interface IPolicyTargetRepo
    {
        /// <summary>
        /// Returns all targets in the repository
        /// </summary>
        IEnumerable<IPolicyTarget> All();

        /// <summary>
        /// returns all policytargets that are satisfied by the specified criteria.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        IEnumerable<IPolicyTarget> All(ISpecification<IPolicyTarget> criteria);
    }
}