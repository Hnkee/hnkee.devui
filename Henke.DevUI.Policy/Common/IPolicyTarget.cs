﻿using System;
using System.Collections.Generic;

namespace Henke.DevUI.Policy.Common
{
    /// <summary>
    /// Used as target for the policychecks
    /// </summary>
    public interface IPolicyTarget
    {
        /// <summary>
        /// Gets the type of the target.
        /// </summary>
        /// <value>The type of the target.</value>
        Type TargetType { get; }

        /// <summary>
        /// Gets the name of the target.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; }

        /// <summary>
        /// Gets the name of the folder.
        /// </summary>
        /// <value>The name of the folder.</value>
        /// <remarks>Assumes that it follows namespaces</remarks>
        string FolderName { get; }

        /// <summary>
        /// Gets the namespace.
        /// </summary>
        /// <value>The namespace.</value>
        string NameSpace { get; }
        
        /// <summary>
        /// All types that are exposed public by the target
        /// </summary>
        /// <example>
        /// If the type has a property like:
        /// public IPerson Person {get;set;} it will consider IPerson to be an ExposedType
        /// </example>
        IEnumerable<Type> AllExposedTypes { get; }
    }
}