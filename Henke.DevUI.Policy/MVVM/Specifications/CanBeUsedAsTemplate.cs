﻿using System.Windows;

namespace Henke.DevUI.Policy.MVVM.Specifications
{
    /// <summary>
    /// Definition if a class can be used for templating
    /// </summary>
    /// <remarks>
    /// I think that everything descending from FrameworkElement will do
    /// </remarks>
    public class CanBeUsedAsTemplate : Inherits<FrameworkElement>
    {

    }
}
