﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.MVVM.Specifications
{
    public class NameEndsWith : Specification<IPolicyTarget>
    {
        public string Suffix { get; private set; }

        public bool IgnoreCase { get; set; }

        public NameEndsWith(string suffix)
        {
            Suffix = suffix;
        }

        public override bool IsSatisfiedBy(IPolicyTarget obj)
        {
            return obj.Name.EndsWith(Suffix, IgnoreCase, null);
        }
    }
}