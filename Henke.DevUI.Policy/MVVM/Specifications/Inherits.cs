﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.MVVM.Specifications
{
    public class Inherits<T> : Specification<IPolicyTarget>
    {
        public override bool IsSatisfiedBy(IPolicyTarget obj)
        {
            return typeof(T).IsAssignableFrom(obj.TargetType);
        }
    }
}