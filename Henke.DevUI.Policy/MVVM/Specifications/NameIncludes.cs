﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.MVVM.Specifications
{
    public class NameIncludes : Specification<IPolicyTarget>
    {
        public string SubString { get; private set; }

        public bool IgnoreCase { get; set; }

        public NameIncludes(string subString)
        {
            SubString = subString;
        }

        public override bool IsSatisfiedBy(IPolicyTarget obj)
        {
            return IgnoreCase ? obj.Name.ToLower().Contains(SubString.ToLower()) : obj.Name.Contains(SubString);
        }
    }
}