﻿using Henke.DevUI.Policy.Common;

namespace Henke.DevUI.Policy.MVVM.Specifications
{
    /// <summary>
    /// We treat namespaces as folderstructures, there should be no good reason not to do so?
    /// </summary>
    /// <remarks>
    /// If so for some special case, override this class
    /// </remarks>
    public class IsInFolder : NameIncludes
    {
        public IsInFolder(string folder)
            : base(folder)
        {

        }

        public override bool IsSatisfiedBy(IPolicyTarget obj)
        {
            return IgnoreCase ? obj.NameSpace.ToLower().Contains(SubString.ToLower()) : obj.NameSpace.Contains(SubString);
        }

    }
}