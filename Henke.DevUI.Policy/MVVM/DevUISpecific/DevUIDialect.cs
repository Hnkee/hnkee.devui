﻿using Henke.DevUI.MVVMUtils;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.Specifications;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.MVVM.DevUISpecific
{
    public class DevUIDialect : IMVVMDialect
    {
        private readonly ICompositeSpecification<IPolicyTarget> _definitionOfAViewModel;
        private readonly ICompositeSpecification<IPolicyTarget> _definitionOfAView;

        public DevUIDialect()
        {
            _definitionOfAViewModel = new NameIncludes("ViewModel").Or(new IsInFolder("ViewModel"));
            _definitionOfAView = new NameIncludes("View").Or(new IsInFolder("View"));
        }

        public  ICompositeSpecification<IPolicyTarget> DefinitionOfAViewModel
        {
            get { return _definitionOfAViewModel; }
        }

        public  ICompositeSpecification<IPolicyTarget> DefinitionOfAView
        {
            get { return _definitionOfAView; }
        }
    }
}
