﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.MVVM.DevUISpecific
{
    public class MVVMNamespaceDefintion : Specification<IPolicyTarget>
    {
        private readonly IPolicyTarget _toBeCompared;

        public MVVMNamespaceDefintion(IPolicyTarget toBeCompared)
        {
            _toBeCompared = toBeCompared;
        }

        public override bool IsSatisfiedBy(IPolicyTarget obj)
        {

            return _toBeCompared.NameSpace.ToLower().Replace("viewmodel", "view") ==
                   obj.NameSpace.ToLower().Replace("viewmodel", "view");

        }

    }
}