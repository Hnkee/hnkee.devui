﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Utils.Specifications;

namespace Henke.DevUI.Policy.MVVM
{
    
    /// <summary>
    /// Definitions for whats valid and not
    /// </summary>
    /// <remarks>
    /// Thought is to have different dialects for different frameworks and styles.
    /// If I want to do stuff like: Give me all viewmodels, we need to have a definition like What is a viewmodel?
    /// You can store your prefernces in a dialect like: My defintion is that it inherits ViewmodelBase, or that its in a folder named ViewModels (or combined etc etc)
    /// </remarks>
    public interface IMVVMDialect
    {

        /// <summary>
        /// Gets the definition of what the system should consider to be a viewmodel
        /// </summary>
        /// <remarks>
        /// All found targets will be tested for validation
        /// </remarks>
        ICompositeSpecification<IPolicyTarget> DefinitionOfAViewModel { get; }

        /// <summary>
        /// Gets the definition of what the system should consider to be a viewmodel
        /// </summary>
        /// <remarks>
        /// All found targets will be tested for validation
        /// </remarks>
        ICompositeSpecification<IPolicyTarget> DefinitionOfAView { get; }
    }
}
