﻿using System.Collections.Generic;
using Henke.DevUI.Policy.Common;

namespace Henke.DevUI.Policy.MVVM
{
    /// <summary>
    /// Extends the IPolicyTargetRepo with MVVM specifics
    /// </summary>
    /// <remarks>
    /// A little Yagni disclaimer: At first, this was put in the TargetRepo interface, 
    /// but in my brain policies started to spread longer than viewmodels so I put it a apearate interface for now.
    /// </remarks>
    public interface IMVVMPolicyTargetRepo:IPolicyTargetRepo
    {
        /// <summary>
        /// All ViewModels
        /// </summary>
        /// <returns>All targets that is classified as ViewModels</returns>
        IEnumerable<IPolicyTarget> AllViewModels();

        /// <summary>
        /// All the views.
        /// </summary>
        /// <returns>All targets that is classified as Views</returns>
        IEnumerable<IPolicyTarget> AllViews();  
    }
}