﻿using System.Collections.Generic;
using System.Reflection;
using Henke.DevUI.Policy.Common;

namespace Henke.DevUI.Policy.MVVM
{
    /// <remarks>
    /// Note 2011-12-01: This will probably change to be a static helper (named All?) used something like: All.ViewModels....
    /// </remarks>
    public class MVVMPolicyTargetRepo : PolicyTargetRepo,IMVVMPolicyTargetRepo
    {
        private readonly IMVVMDialect _dialect;

        /// <summary>
        /// Initializes a new instance of the <see cref="MVVMPolicyTargetRepo"/> class.
        /// </summary>
        /// <param name="targetAssembly">The target assembly.</param>
        /// <param name="dialect">The dialect.</param>
        public MVVMPolicyTargetRepo(IAssembly targetAssembly, IMVVMDialect dialect) : base(targetAssembly)
        {
            _dialect = dialect;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MVVMPolicyTargetRepo"/> class.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <param name="dialect">The dialect.</param>
        public MVVMPolicyTargetRepo(Assembly assembly, IMVVMDialect dialect) : base(assembly)
        {
            _dialect = dialect;
        }

        /// <summary>
        /// All ViewModels
        /// </summary>
        /// <returns>
        /// All targets that is classified as ViewModels (by the dialect)
        /// </returns>
        public IEnumerable<IPolicyTarget> AllViewModels()
        {
            return All(_dialect.DefinitionOfAViewModel);
        }

        /// <summary>
        /// All the views.
        /// </summary>
        /// <returns>All targets that is classified as Views (by the dialect)</returns>
        public IEnumerable<IPolicyTarget> AllViews()
        {
            return All(_dialect.DefinitionOfAView);
        }

    }
}