﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;

namespace Henke.DevUI.Controls.Behaviors
{
    /// <summary>
    /// Shows a simple popup when triggered, content can be anything
    /// </summary>
    /// <remarks>
    /// When SkinnablePopup is finished, swap to usin that instead.
    /// </remarks>
    public class ShowPopupAction : TriggerAction<FrameworkElement>
    {
        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }

        public static readonly DependencyProperty ContentProperty =
            DependencyProperty.Register("Content", typeof(object), typeof(ShowPopupAction), new UIPropertyMetadata(" ! "));

        protected override void Invoke(object parameter)
        {
                    new Popup
                              {
                                  PlacementTarget = AssociatedObject,
                                  StaysOpen = false,
                                  Child = new ContentControl() {Content = Content},
                                  IsOpen = true
                              };
        }
    }
}
