﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Henke.DevUI.Controls.Behaviors.ShowHide
{
    public class ToggleVisibilityCommand : ICommand
    {
        private readonly FrameworkElement _target;

        public ToggleVisibilityCommand(FrameworkElement target)
        {
            _target = target;
        }

        public void Execute(object parameter)
        {
            _target.Visibility = _target.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}