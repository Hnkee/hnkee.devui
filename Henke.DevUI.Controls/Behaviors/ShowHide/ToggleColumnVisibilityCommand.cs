﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Henke.DevUI.Controls.Behaviors.ShowHide
{
    public class ToggleColumnVisibilityCommand : ICommand
    {
        private readonly DataGridColumn _target;
        private readonly Visibility _visibility;

        public ToggleColumnVisibilityCommand(DataGridColumn target)
        {
            _target = target;
            _visibility = _target.Visibility == Visibility.Hidden ? Visibility.Hidden : Visibility.Collapsed;
        }

        public void Execute(object parameter)
        {
            _target.Visibility = _target.Visibility == Visibility.Visible ? _visibility : Visibility.Visible;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}