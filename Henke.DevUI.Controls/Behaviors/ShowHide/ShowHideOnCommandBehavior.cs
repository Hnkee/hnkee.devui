﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Henke.DevUI.Controls.Behaviors.ShowHide
{
    /// <summary>
    /// Creates a contextmenu for hiding an object
    /// </summary>
    /// <remarks>
    /// Remmeber: If a user hides the object with the contextmenu they cannot use a contextmenu to show it again since its hidden!
    /// </remarks>
    public class ShowHideOnCommandBehavior : Behavior<FrameworkElement>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            if (AssociatedObject.ContextMenu == null) AssociatedObject.ContextMenu = new ContextMenu();
            AssociatedObject.ContextMenu.Items.Add(new MenuItem() { Header = Header, Command = new ToggleVisibilityCommand(AssociatedObject) });
        }

        public object Header
        {
            get { return (object)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("HeaderProperty", typeof(object), typeof(ShowHideOnCommandBehavior), new UIPropertyMetadata("Remove"));


    }
}