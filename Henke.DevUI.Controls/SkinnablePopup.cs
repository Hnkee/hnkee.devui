﻿using System.Windows;
using System.Windows.Controls;

namespace Henke.DevUI.Controls
{
    //http://blogs.msdn.com/b/wpfsdk/archive/2007/04/27/popup-your-control.aspx
    public class SkinnablePopup : Control
    {
        static SkinnablePopup()
        {         
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SkinnablePopup), new FrameworkPropertyMetadata(typeof(SkinnablePopup)));
        }
    }
}
