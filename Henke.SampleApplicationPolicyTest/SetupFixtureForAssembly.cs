﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.DevUISpecific;
using Henke.SampleApplication.Domain;
using NUnit.Framework;

namespace Henke.SampleApplicationPolicyTest
{
    [SetUpFixture]
    public class SetupFixtureForAssembly
    {

        [SetUp]
        public void RunOnceBeforeAnyTestIsExecuted()
        {
            Policy.CreateRepo(typeof(BacklogItem).Assembly,new DevUIDialect());
        }

    }
}
