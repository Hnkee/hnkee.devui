﻿using System.Reflection;
using Henke.DevUI.Policy.MVVM;

namespace Henke.SampleApplicationPolicyTest
{
    public static class Policy
    {
        private static MVVMPolicyTargetRepo _targetRepo;

        public static void CreateRepo(Assembly assembly,IMVVMDialect dialect)
        {
            _targetRepo = new MVVMPolicyTargetRepo(assembly, dialect);
        }

        public static IMVVMPolicyTargetRepo TargetRepo
        {
            get { return _targetRepo; }
        }

    }
}