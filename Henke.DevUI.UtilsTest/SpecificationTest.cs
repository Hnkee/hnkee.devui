﻿using Henke.DevUI.Utils.Specifications;
using NUnit.Framework;

namespace Henke.DevUI.UtilsTest
{
    [TestFixture]
    public class SpecificationTest
    {
        private Specification<string> _falseSpec;
        private Specification<string> _trueSpec;

        [SetUp]
        public void Setup()
        {
            _falseSpec = new SimpleSpecificationForTest(false);
            _trueSpec = new SimpleSpecificationForTest(true);
        }

        #region OR
        [Test]
        public void IsSatisfied_WhenOneOrMoreSpecificationAreTrueUsingOr_ShouldReturnTrue()
        {
            Assert.That(_falseSpec.Or(_trueSpec).IsSatisfiedBy("value"), "Should be true because second specification is satisfied");
            Assert.That(_trueSpec.Or(_falseSpec).IsSatisfiedBy("value"), "Should be true because first specification is satisfied");
            Assert.That(_trueSpec.Or(_trueSpec).IsSatisfiedBy("value"), "Should be true because both specifications are satisfied");
        }

        [Test]
        public void IsSatisfied_WhenNoSpecificationAreTrueUsingOr_ShouldReturnFalse()
        {
            Assert.That(_falseSpec.Or(_falseSpec).IsSatisfiedBy("value"), Is.False);

        }
        #endregion

        #region AND
        [Test]
        public void IsSatisfied_WhenAllSpecificationAreTrueUsingAnd_ShouldReturnTrue()
        {
            Assert.That(_trueSpec.And(_trueSpec).IsSatisfiedBy("value"));
        }

        [Test]
        public void IsSatisfied_WhenOneOrMoreSpecificationAreFalseUsingAnd_ShouldReturnFalse()
        {
            Assert.That(_falseSpec.And(_trueSpec).IsSatisfiedBy("value"), Is.False, "Should be false because first specification is not satisfied");
            Assert.That(_trueSpec.And(_falseSpec).IsSatisfiedBy("value"), Is.False, "Should be false because second specification is not satisfied");
            Assert.That(_falseSpec.And(_trueSpec).IsSatisfiedBy("value"), Is.False, "Should be false because no specifications are satisfied");
        }

        #endregion

        #region ANDNOT
        [Test]
        public void IsSatisfied_WhenAndNotIsSatisfied_ShouldReturnFalse()
        {
            Assert.That(_trueSpec.AndNot(_trueSpec).IsSatisfiedBy("value"), Is.False);
        }

        [Test]
        public void IsSatisfied_WhenFirstSpecificationIsntSatisfiesd_ShouldReturnFalse()
        {
            Assert.That(_falseSpec.AndNot(_trueSpec).IsSatisfiedBy("value"), Is.False);
            Assert.That(_falseSpec.AndNot(_falseSpec).IsSatisfiedBy("value"), Is.False);
        }

        [Test]
        public void IsSatisfied_WhenAndNotIsNotSatisfied_ShouldReturnTrue()
        {
            Assert.That(_trueSpec.AndNot(_falseSpec).IsSatisfiedBy("value"));
        }
        #endregion

        private class SimpleSpecificationForTest : Specification<string>
        {
            private readonly bool _isSatisfied;

            public SimpleSpecificationForTest(bool isSatisfied)
            {
                _isSatisfied = isSatisfied;
            }

            public override bool IsSatisfiedBy(string obj)
            {
                return _isSatisfied;
            }
        }
    }
}
