﻿namespace Henke.DevUI.Utils.Specifications
{
    public interface ICompositeSpecification<T> : ISpecification<T>
    {
        ICompositeSpecification<T> And(ISpecification<T> specification);

        ICompositeSpecification<T> Or(ISpecification<T> specification);

        ICompositeSpecification<T> AndNot(ISpecification<T> specification);
    }
}