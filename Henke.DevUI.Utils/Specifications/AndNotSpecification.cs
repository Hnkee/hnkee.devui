﻿namespace Henke.DevUI.Utils.Specifications
{
    internal class AndNotSpecification<T> : CompositeSpecification<T>
    {
        internal AndNotSpecification(ISpecification<T> first, ISpecification<T> second)
            : base(first, second)
        {
        }

        public override bool IsSatisfiedBy(T obj)
        {
            return _first.IsSatisfiedBy(obj) && !_second.IsSatisfiedBy(obj);
        }
    }
}