﻿namespace Henke.DevUI.Utils.Specifications
{
    public abstract class Specification<T> : ICompositeSpecification<T>
    {
        public abstract bool IsSatisfiedBy(T obj);

        public ICompositeSpecification<T> And(ISpecification<T> specification)
        {
            return new AndSpecification<T>(this, specification);
        }

        public ICompositeSpecification<T> Or(ISpecification<T> specification)
        {
            return new OrSpecification<T>(this, specification);
        }

        public ICompositeSpecification<T> AndNot(ISpecification<T> specification)
        {
            return new AndNotSpecification<T>(this, specification);
        }
    }
}