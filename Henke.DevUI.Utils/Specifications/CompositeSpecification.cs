﻿namespace Henke.DevUI.Utils.Specifications
{
    internal abstract class CompositeSpecification<T> : Specification<T>
    {
        protected readonly ISpecification<T> _first;
        protected readonly ISpecification<T> _second;

        protected CompositeSpecification(ISpecification<T> first, ISpecification<T> second)
        {
            _first = first;
            _second = second;
        }
    }
}