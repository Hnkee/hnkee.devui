﻿using Henke.DevUI.Utils.Specifications;
using NUnit.Framework.Constraints;

namespace Henke.DevUI.TestUtils.Common
{
    public static class SpecificationToConstraint
    {
        /// <summary>
        /// Creates an NUnit constraint from a specification.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="specification">The specification.</param>
        /// <returns></returns>
        public static Constraint ToConstraint<T>(this ISpecification<T> specification)
        {
            return new SpecConstraint<T>(specification);
        }

        private class SpecConstraint<T> : Constraint
        {
            private readonly ISpecification<T> _specification;

            public SpecConstraint(ISpecification<T> specification)
            {
                _specification = specification;
            }

            public override bool Matches(object actual)
            {
                return _specification.IsSatisfiedBy((T)actual);
            }

            public override void WriteDescriptionTo(MessageWriter writer)
            {
                //TODO
            }

        }
    }
}
