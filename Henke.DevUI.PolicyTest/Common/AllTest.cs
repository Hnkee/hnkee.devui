﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Henke.DevUI.MVVMUtils;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM;
using Henke.DevUI.Utils.Specifications;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.Common
{
    [TestFixture]
    public class AllTest
    {
        [SetUp]
        public void Setup()
        {
            All.Dialect = new DialectForTest();    
        }

        [Test]
        public void WhenLoadIsNotCalled_ItShouldDefaultLoadReferencedAssembliesWithSameNameSpace()
        {
            All.TargetAssemblies = null; 
            Assert.That(HasLoadedPolicyAssembly, "PolicyTarget should have been loaded since we have a ref to Henke.DevUI.Policy");
            Assert.That(HasLoadedMVVMUtilsAssembly, "ViewModelBase should have been loaded since we have a ref to Henke.DevUI.MVVMUtils");
        }

        [Test]
        public void Load_ShouldSetTheTargetAssembly()
        {          
            All.TargetAssemblies = new List<Assembly>(){GetType().Assembly};
            Assert.That(All.ViewModels.Count(vm => vm.TargetType == GetType()), Is.EqualTo(1));
            Assert.That(HasLoadedPolicyAssembly, Is.False);
            Assert.That(HasLoadedMVVMUtilsAssembly, Is.False);
        }

        private static bool HasLoadedMVVMUtilsAssembly
        {
            get { return All.ViewModels.Count(vm => vm.TargetType.Equals(typeof(ViewModelBase))) > 0; }
        }

        private static bool HasLoadedPolicyAssembly
        {
            get
            {
                return All.ViewModels.Count(vm => vm.TargetType.Equals(typeof(PolicyTarget))) > 0;
            }
        }

        internal class DialectForTest:IMVVMDialect
        {
            private readonly ICompositeSpecification<IPolicyTarget> _alwaysSatisfiedSpecification =
                new AlwaySatisfiedSpecification<IPolicyTarget>();
            
            public ICompositeSpecification<IPolicyTarget> DefinitionOfAViewModel
            {
                get { return _alwaysSatisfiedSpecification; }
            }

            public ICompositeSpecification<IPolicyTarget> DefinitionOfAView
            {
                get { return _alwaysSatisfiedSpecification; }
            }

            internal class AlwaySatisfiedSpecification<T> : Specification<IPolicyTarget>
            {
                public override bool IsSatisfiedBy(IPolicyTarget obj)
                {
                    return true;
                }

            }
        }
    }

   
}
