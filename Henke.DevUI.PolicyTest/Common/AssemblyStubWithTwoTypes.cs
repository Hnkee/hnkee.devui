﻿using System;
using System.Collections.Generic;
using Henke.DevUI.Policy.Common;

namespace Henke.DevUI.PolicyTest.Common
{

    public abstract class AssemblyStubWithFixedTypes:IAssembly
    {
        private readonly IList<Type> _allTypesInAssembly;

        protected AssemblyStubWithFixedTypes(IList<Type> allTypesInAssembly)
        {
            _allTypesInAssembly = allTypesInAssembly;
        }

        public  int NumberOfTypes
        {
            get { return _allTypesInAssembly.Count; }
        }

        public IList<Type> AllTypes()
        {
            return _allTypesInAssembly;
        }

    }
    /// <summary>
    /// A stub that exposes two different public types
    /// </summary>
    internal class AssemblyStubWithTwoTypes : AssemblyStubWithFixedTypes
    {   
        public AssemblyStubWithTwoTypes()
            : base(new List<Type>() { typeof(ClassOne), typeof(ClassTwo) })
        {
            
        }

        #region helpers to clearify testsyntax
        public Type FirstType
        {
            get { return typeof(ClassOne); }
        }

        public Type SecondType
        {
            get { return typeof(ClassTwo); }
        }

        #endregion

    }


    internal class AssemblyStubWithOneType : AssemblyStubWithFixedTypes
    {

        public AssemblyStubWithOneType():base(new List<Type>(){typeof(ClassThree)})
        {
            
        }
    }

    internal class ClassOne
    {

    }

    internal class ClassTwo
    {

    }

    internal class ClassThree
    {
        
    }
}