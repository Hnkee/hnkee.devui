﻿using System.Collections.Generic;
using System.Linq;
using Henke.DevUI.Policy.Common;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.Common
{
    [TestFixture]
    public class PolicyTargetTest
    {
        private PolicyTarget _target;

        [SetUp]
        public void Setup()
        {
            _target = new PolicyTarget(typeof(PolicyTargetTest));
            Assert.That(typeof(PolicyTargetTest).Namespace, Is.EqualTo("Henke.DevUI.PolicyTest.Common"), "Assumption for all tests in this class");
        }

        [Test]
        public void FolderName_ShouldBeTheLastPartOfNameSpace()
        {
            Assert.That(_target.FolderName, Is.EqualTo("Common"));
        }

        [Test]
        public void NameSpace_ShouldBeTheCompleteNameSpace()
        {
            Assert.That(_target.NameSpace, Is.EqualTo("Henke.DevUI.PolicyTest.Common"));
        }

        [Test]
        public void ExposedTypes_ShouldReturnAllPublicProperties()
        {
            _target = new PolicyTarget(typeof(TestTargetWithPublicStringProp));
            Assert.That(_target.AllExposedTypes.Single(),Is.EqualTo(typeof(string)));
        }

        [Test]
        public void ExposedTypes_ShouldReturnAllPublicFields()
        {
            _target = new PolicyTarget(typeof(TestTargetWithPublicStringField));
            Assert.That(_target.AllExposedTypes.Single(), Is.EqualTo(typeof(string)));
        }

        [Test]
        public void ExposedTypes_ShouldReturnGenericsAsSeparateTypes()
        {
            _target = new PolicyTarget(typeof(TestTargetWithTwoGenericStringsAndOneGenericInt));
          Assert.That(_target.AllExposedTypes.Count(),Is.EqualTo(2));
          Assert.That(_target.AllExposedTypes.Contains(typeof(IList<string>)));
          Assert.That(_target.AllExposedTypes.Contains(typeof(IList<int>)));
        }

        internal class TestTargetWithPublicStringProp
        {
            public string PublicStringProp { get; set; }
            private int PrivateIntProp { get; set; }
        }

        internal class TestTargetWithPublicStringField
        {
            public string PublicStringProp;
            private int PrivateIntProp;
        }

        internal class TestTargetWithTwoGenericStringsAndOneGenericInt
        {
            public IList<string> FirstListOfStrings { get; set; }
            public IList<string> SecondListOfStrings { get; set; }
            public IList<int> ListOfInts { get; set; }
        }
    }
}