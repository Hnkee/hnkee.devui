﻿using System;
using Henke.DevUI.Policy.Common;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.Common
{
    [TestFixture]
    public class RemoveTest
    {
        [Test]
        public void  LastDotBefore_ShouldRemoveTheLastDotBeforeParameterAndEverythingAfter()
        {
            const string parameter = "Param";
            const string beginning = "someStringContaining.StuffAfter";
            var targetStringContainingParameter = String.Format("{0}.SomeDotsXXXXXx{1}xxxXXXX", beginning, parameter);          
            var result = Remove.LastDotBefore(parameter,targetStringContainingParameter);
            Assert.That(result,Is.EqualTo(beginning));
        }

        [Test]
        public void LastDotBefore_WhenParameterCannotBeFoundInTarget_ShouldReturnCompleteTarget()
        {
            const string parameter = "xxxxxx";
            const string target = "someStringNotContaingTheParameter";
            var result = Remove.LastDotBefore(parameter, target);
            Assert.That(result, Is.EqualTo(target));
        }

        [Test]
        public void LastDotBefore_WhenTargetDoesNotContainADotbeforeParameter_ShouldReturnCompleteTargetBeforeParameter()
        {
            const string parameter = "Param";
            const string beginning = "stringWithoutADotBefore";
            var targetStringContainingParameter = String.Format("{0}{1}someTextThjatShouldBeremoved", beginning, parameter);
            var result = Remove.LastDotBefore(parameter, targetStringContainingParameter);
            Assert.That(result, Is.EqualTo(beginning));
        }
    }
}