﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Utils.Specifications;
using NUnit.Framework;
using Rhino.Mocks;

namespace Henke.DevUI.PolicyTest.Common
{
    [TestFixture]
    public class PolicyTargetRepoTest
    {
        private AssemblyStubWithTwoTypes _assembly;
        private PolicyTargetRepo _target;

        [SetUp]
        public void Setup()
        {
            _assembly = new AssemblyStubWithTwoTypes();
            _target = new PolicyTargetRepo(_assembly);
        }

        [Test]
        public void All_WhenCalled_ShouldCreatePolicyTargetOfEachTypeInTheAssembly()
        {
            var result = _target.All();
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result.Count(t => t.TargetType.Equals(_assembly.FirstType)), Is.EqualTo(1));
            Assert.That(result.Count(t => t.TargetType.Equals(_assembly.SecondType)), Is.EqualTo(1));
        }

        [Test]
        public void All_WhenCalledWithSpecification_ShouldCheckEachTargetAgainstTheSpecification()
        {
            var mockRepo = new MockRepository();
            var specification = mockRepo.StrictMock<ISpecification<IPolicyTarget>>();
            using (mockRepo.Record())
            {
                Expect.Call(specification.IsSatisfiedBy(null)).Return(false).IgnoreArguments().Repeat.Twice();

            }
            using (mockRepo.Playback())
            {
                CollectionAssert.IsEmpty(_target.All(specification));
            }
        }

        [Test]
        public void All_WhenCalledWithSpecification_ShouldReturnAllTargetSatisfiedByTheSpecification()
        {
            var mockRepo = new MockRepository();
            var specification = mockRepo.StrictMock<ISpecification<IPolicyTarget>>();
            using (mockRepo.Record())
            {
                Expect.Call(specification.IsSatisfiedBy(null)).Return(true).IgnoreArguments().Repeat.Twice();
            }
            using (mockRepo.Playback())
            {
                Assert.That(_target.All(specification).Count(), Is.EqualTo(2));
            }
        }

        [Test]
        public void All_WhenCalled_ShouldCreatePolicyTargetOfEachTypeInTheAssemblies()
        {
            var firstAssembly = new AssemblyStubWithTwoTypes();
            var secondAssembly = new AssemblyStubWithOneType();
            var assemblies = new List<IAssembly>() { firstAssembly, secondAssembly };

            _target = new PolicyTargetRepo(assemblies);

            Assert.That(_target.All().Count(), Is.EqualTo(firstAssembly.NumberOfTypes + secondAssembly.NumberOfTypes));
        }

        [Test]
        public void All_WhenCreatedWithAnActualAssembly_ShouldCreateTargetsFromThatAssembly()
        {
            _target = new PolicyTargetRepo(GetType().Assembly);
            Assert.That(_target.All().Count(t => t.TargetType.Equals(GetType())), Is.EqualTo(1));
        }

        [Test]
        public void All_WhenCreatedWithAlistOfActualAssemblies_ShouldCreateTargetsFromThoseAssemblies()
        {
            var assemblies = new List<Assembly>() { GetType().Assembly, _target.GetType().Assembly };
            _target = new PolicyTargetRepo(assemblies);
            Assert.That(_target.All().Count(t => t.TargetType.Equals(GetType()) || t.TargetType.Equals(_target.GetType())), Is.EqualTo(2));
        }

    }
}
