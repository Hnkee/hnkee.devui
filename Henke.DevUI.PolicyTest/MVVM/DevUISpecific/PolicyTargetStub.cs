﻿using System;
using System.Collections.Generic;
using Henke.DevUI.Policy.Common;

namespace Henke.DevUI.PolicyTest.MVVM.DevUISpecific
{
    internal class PolicyTargetStub : IPolicyTarget
    {
        public Type TargetType { get; set; }
        public string Name { get; set; }
        public string FolderName { get; set; }
        public string NameSpace { get; set; }
        public IEnumerable<Type> AllExposedTypes { get; set; }
       
    }
}