﻿using Henke.DevUI.Policy.MVVM.DevUISpecific;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.MVVM.DevUISpecific
{
    [TestFixture]
    public class HasSameNamespaceStructureTest
    {
        private string _nameSpaceBase;
        private string _subFolder;

        [SetUp]
        public void Setup()
        {
            _nameSpaceBase = "MyNameSpace.FolderOne.FolderTwo";
            _subFolder = ".Subfolder";

        }

        [Test]
        public void IsSatisfied_WhenNamespaceAreEqual_ShouldReturnTrue()
        {
            var viewModel = new PolicyTargetStub() { NameSpace = _nameSpaceBase };
            var view = new PolicyTargetStub() { NameSpace = _nameSpaceBase };
            var target = new MVVMNamespaceDefintion(viewModel);
            Assert.That(target.IsSatisfiedBy(view));
        }

        [Test]
        public void IsSatisfied_WhenNamespaceAreNotEqual_ShouldReturnFalse()
        {
            var viewModel = new PolicyTargetStub() { NameSpace = _nameSpaceBase };
            var view = new PolicyTargetStub() { NameSpace = _nameSpaceBase + "NotEqual" };
            var target = new MVVMNamespaceDefintion(viewModel);
            Assert.That(target.IsSatisfiedBy(view), Is.False);
        }

        [Test]
        public void IsSatisfiedByView_WhenNamespaceOnlyDiffByViewAndViewModel_ShouldReturnTrue()
        {
            var viewModel = new PolicyTargetStub() { NameSpace = _nameSpaceBase + ".ViewModel" + _subFolder };
            var view = new PolicyTargetStub() { NameSpace = _nameSpaceBase + ".View" + _subFolder };
            var target = new MVVMNamespaceDefintion(viewModel);
            Assert.That(target.IsSatisfiedBy(view));
        }

        [Test]
        public void IsSatisfiedByViewModel_WhenNamespaceOnlyDiffByViewAndViewModel_ShouldReturnTrue()
        {

            var viewModel = new PolicyTargetStub() { NameSpace = _nameSpaceBase + ".ViewModel" + _subFolder };
            var view = new PolicyTargetStub() { NameSpace = _nameSpaceBase + ".View" + _subFolder };
            var target = new MVVMNamespaceDefintion(viewModel);
            Assert.That(target.IsSatisfiedBy(view));
        }
        [Test]
        public void IsSatisfied_WhenNamespaceOnlyDiffByViewAndViewModelAndCase_ShouldReturnTrue()
        {
            var viewModel = new PolicyTargetStub() { NameSpace = _nameSpaceBase + ".viewmodel" + _subFolder };
            var view = new PolicyTargetStub() { NameSpace = _nameSpaceBase + ".View" + _subFolder };
            var target = new MVVMNamespaceDefintion(viewModel);
            Assert.That(target.IsSatisfiedBy(view));
        }
    }
}
