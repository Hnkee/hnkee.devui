﻿using System.Windows;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.Specifications;
using NUnit.Framework;
using Rhino.Mocks;

namespace Henke.DevUI.PolicyTest.MVVM.Specifications
{
    [TestFixture]
    public class CanBeUsedAsTemplateTest
    {
        private CanBeUsedAsTemplate _target;
        private IPolicyTarget _policyTarget;

        [SetUp]
        public void Setup()
        {
            _target = new CanBeUsedAsTemplate();
            _policyTarget = MockRepository.GenerateStub<IPolicyTarget>();
        }

        [Test]
        public void IsSatisfied_WhenTargetInheritsFrameworkElement_ShouldReturnTrue()
        {
            _policyTarget.Stub(x => x.TargetType).Return(typeof(FrameworkElement));
            Assert.That(_target.IsSatisfiedBy(_policyTarget));
        }

        [Test]
        public void IsSatisfied_WhenTargetDoesNotInheritsFrameworkElement_ShouldReturnFalse()
        {
            _policyTarget.Stub(x => x.TargetType).Return(typeof(CanBeUsedAsTemplateTest));
            Assert.That(_target.IsSatisfiedBy(_policyTarget), Is.False);
        }
    }
}
