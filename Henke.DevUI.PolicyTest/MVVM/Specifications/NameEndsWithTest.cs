﻿using Henke.DevUI.MVVMUtils;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.Specifications;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.MVVM.Specifications
{
    [TestFixture]
    public class NameEndsWithTest
    {
        private NameEndsWith _target;

        private IPolicyTarget _validViewModelTarget;
        private IPolicyTarget _alsoValidViewModelTarget;
        private IPolicyTarget _targetWithoutViewModelInTheName;
        private IPolicyTarget _targetWithoutViewModelInTheEndOfTheName;
        private IPolicyTarget _targetWithWrongCasing;

        [SetUp]
        public void Setup()
        {
            _target = new NameEndsWith("ViewModel");
            _validViewModelTarget = new PolicyTarget(typeof(ThisAViewModel));
            _alsoValidViewModelTarget = new PolicyTarget(typeof(ThisIsAlsoAViewModel));
            _targetWithoutViewModelInTheName = new PolicyTarget(typeof(NotAVM));
            _targetWithoutViewModelInTheEndOfTheName = new PolicyTarget(typeof(ThisIsAViewModelNot));
            _targetWithWrongCasing = new PolicyTarget(typeof(CaseSensitiveViewmodel));


        }

        [Test]
        public void IsSatisfied_ShouldReturnTrue_IfTypeEndsWithViewModel()
        {
            Assert.That(_target.IsSatisfiedBy(_validViewModelTarget), Is.True);
            Assert.That(_target.IsSatisfiedBy(_alsoValidViewModelTarget), Is.True);
        }


        [Test]
        public void IsSatisfied_ShouldReturnFalse_IfTypeDoesNotEndsWithViewModel()
        {
            Assert.That(_target.IsSatisfiedBy(_targetWithoutViewModelInTheName), Is.False);
            Assert.That(_target.IsSatisfiedBy(_targetWithoutViewModelInTheEndOfTheName), Is.False);
        }

        [Test]
        public void IsSatisfied_ShouldReturnFalse_IfCaseIsNotCorrect()
        {
            Assert.That(_target.IsSatisfiedBy(_targetWithWrongCasing), Is.False);
        }

        [Test]
        public void IsSatisfied_ShouldReturnTrue_IfCaseIsNotCorrectButCaseSensitiveIsSetToFalse()
        {
            _target.IgnoreCase = true;
            Assert.That(_target.IsSatisfiedBy(_targetWithWrongCasing));
        }



        #region targetclasses

        private class NotAVM
        {

        }

        private class ThisAViewModel
        {

        }

        private class ThisIsAlsoAViewModel : ViewModelBase
        {

        }

        private class ThisIsAViewModelNot
        {

        }

        /// <summary>
        /// small m in model
        /// </summary>
        private class CaseSensitiveViewmodel
        {

        }

        #endregion

    }
}