﻿using System;
using System.Collections.Generic;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.Specifications;
using Henke.DevUI.PolicyTest.MVVM.DevUISpecific;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.MVVM.Specifications
{
    [TestFixture]
    public class ExposesTest
    {
        private IPolicyTarget _targetItem;

        [Test]
        public void IsSatified_WhenTargetHasPublicPropertyOfTheType_ShouldReturnTrue()
        {
            CreateTargetWithPublicTypes(typeof(string));
            
            Assert.That(new Exposes(typeof(int)).IsSatisfiedBy(_targetItem),Is.False);
            Assert.That(new Exposes(typeof(string)).IsSatisfiedBy(_targetItem));
        }

        [Test]
        public void IsSatisfied_WhenTargetHasPublicPropertyThatInheritsType_ShouldReturnTrue()
        {
            CreateTargetWithPublicTypes(typeof(TestImplementation));

            Assert.That(new Exposes(typeof(TestImplementation)).IsSatisfiedBy(_targetItem), Is.True, "because it exposes the actual type");
            Assert.That(new Exposes(typeof(DerivedTestImplementation)).IsSatisfiedBy(_targetItem), Is.False, "because it exposes a derived type");
            Assert.That(new Exposes(typeof(ITestInterface)).IsSatisfiedBy(_targetItem), Is.True, "because it exposes an interface of that type");
        }

        [Test]
        public void IsSatisfied_WhenTargetHasGenericTypeThatHasArgumentThatImplementsTheType_ShouldReturnTrue()
        {
            CreateTargetWithPublicTypes(typeof(IList<DerivedTestImplementation>));
            Assert.That(new Exposes(typeof(IList<TestImplementation>)).IsSatisfiedBy(_targetItem), Is.True, "because we are exposing a list of DerivedTestImplementation, thats assignable from TestImplementation");
        }

        //TODO: Handle generics
        [Test,Ignore]
        public void IsSatisfied_WhenTargetHasGenericTypeThatImplementsTheType_ShouldReturnTrue()
        {
            CreateTargetWithPublicTypes(typeof(List<object>));
            Assert.That(new Exposes(typeof(IList<object>)).IsSatisfiedBy(_targetItem), Is.True, "because we are exposing a list of DerivedTestImplementation, thats assignable from TestImplementation");
        }

        [Test]
        public void IsSatisfied_WhenTheSpecificationsTypeIsNotAssignableFromTheTargetsType_ShouldReturnFalse()
        {
            CreateTargetWithPublicTypes(typeof(IList<TestImplementation>));
            Assert.That(new Exposes(typeof(IList<DerivedTestImplementation>)).IsSatisfiedBy(_targetItem),Is.False, "because we are exposing a list of TestImplementation, not DerivedTestImplementations");
        }

        private void CreateTargetWithPublicTypes(IEnumerable<Type> publicTypes)
        {
            _targetItem = new PolicyTargetStub() { AllExposedTypes = publicTypes };
        }

        private void CreateTargetWithPublicTypes(Type publicType)
        {
            CreateTargetWithPublicTypes(new List<Type>() { publicType });
        }

        #region testclasses
        internal interface ITestInterface
        {

        }

        internal class TestImplementation : ITestInterface
        {

        }

        internal class DerivedTestImplementation : TestImplementation
        {

        }
       
        #endregion
    }





}
