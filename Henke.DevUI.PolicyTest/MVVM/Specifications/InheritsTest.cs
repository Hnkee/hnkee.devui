﻿using Henke.DevUI.MVVMUtils;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.Specifications;
using NUnit.Framework;

namespace Henke.DevUI.PolicyTest.MVVM.Specifications
{
    [TestFixture]
    public class InheritsTest
    {

        private IPolicyTarget _notOkViewModel;
        private IPolicyTarget _okViewModel;
        private Inherits<ViewModelBase> _target;

        [SetUp]
        public void Setup()
        {
            _target = new Inherits<ViewModelBase>();
            _notOkViewModel = new PolicyTarget(typeof(NotOkViewModel));
            _okViewModel = new PolicyTarget(typeof(OkViewModel));
        }

        [Test]
        public void IsSatisfiedBy_WhenTargetDoesNotInheritViewModelBase_ShouldReturnFalse()
        {
            Assert.That(_target.IsSatisfiedBy(_notOkViewModel), Is.False);
        }

        [Test]
        public void IsSatisfiedBy_WhenTargetInheritViewModelBase_ShouldreturnTrue()
        {
            Assert.That(_target.IsSatisfiedBy(_okViewModel));
        }

        #region helpers
        private class OkViewModel : ViewModelBase
        {

        }


        private abstract class SomeOtherViewModelBase
        {

        }

        private class NotOkViewModel : SomeOtherViewModelBase
        {

        }
        #endregion
    }
}