﻿using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM.Specifications;
using NUnit.Framework;
using Rhino.Mocks;

namespace Henke.DevUI.PolicyTest.MVVM.Specifications
{
    [TestFixture]
    public class NameIncludesTest
    {
        private IPolicyTarget _policyTarget;
        private MockRepository _mocks;
        private string _name;
        private NameIncludes _target;

        [SetUp]
        public void Setup()
        {
            _mocks = new MockRepository();
            _name = "ViewModel";
            _mocks = new MockRepository();
            _target = new NameIncludes(_name);
        }

        [Test]
        public void IsSatisfied_WhenNameIsSame_ShouldReturnTrue()
        {
            SetTypeNameTo(_name);
            Assert.IsTrue(TargetIsSatisfied);
        }

        [Test]
        public void IsSatisfied_WhenNameIsIncludedInTheTargetsName()
        {
            SetTypeNameTo("Prefix" + _name + "Suffix");
            Assert.IsTrue(TargetIsSatisfied);
        }

        [Test]
        public void Issatisfied_WhenNameIsNaotCasedCorrectly_ShouldReturnFalse()
        {
            Assert.That(_name, Is.Not.EqualTo(_name.ToLower()), "Name cannot be all lower to perform test correct");
            SetTypeNameTo(_name.ToLower());
            Assert.IsTrue(TargetIsNotSatisfied);
        }

        private void SetTypeNameTo(string name)
        {
            _policyTarget = _mocks.StrictMock<IPolicyTarget>();
            using (_mocks.Record())
            {
                Expect.Call(_policyTarget.Name).Return(name);
            }
        }

        private bool TargetIsNotSatisfied
        {
            get { return !TargetIsSatisfied; }
        }

        private bool TargetIsSatisfied
        {
            get
            {
                using (_mocks.Playback())
                {
                    return _target.IsSatisfiedBy(_policyTarget);
                }
            }

        }
    }
}