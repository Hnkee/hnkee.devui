﻿using System.Linq;
using Henke.DevUI.Policy.Common;
using Henke.DevUI.Policy.MVVM;
using Henke.DevUI.PolicyTest.Common;
using Henke.DevUI.Utils.Specifications;
using NUnit.Framework;
using Rhino.Mocks;

namespace Henke.DevUI.PolicyTest.MVVM
{
    [TestFixture]
    public class MVVMPolicyTargetRepoTest
    {
        private MockRepository _mocks;
        private AssemblyStubWithTwoTypes _assembly;
        private IMVVMDialect _mockedDialect;
        private MVVMPolicyTargetRepo _target;
        private ICompositeSpecification<IPolicyTarget> _alwaysSatisfied;
        private ICompositeSpecification<IPolicyTarget> _neverSatisfied;

        [SetUp]
        public void Setup()
        {  
            _mocks = new MockRepository();
            _mockedDialect = _mocks.StrictMock<IMVVMDialect>();
            _assembly = new AssemblyStubWithTwoTypes();
            _target = new MVVMPolicyTargetRepo(_assembly, _mockedDialect);
            _alwaysSatisfied = new SpecificationStub(true);
            _neverSatisfied = new SpecificationStub(false);
        }

        [Test]
        public void AllViewModels_ShouldReturnAllPolicyTargetsThatAreConsideredAsViewModelsByTheDialect()
        {
            using (_mocks.Record())
            {
                Expect.Call(_mockedDialect.DefinitionOfAViewModel).Return(_alwaysSatisfied);
                Expect.Call(_mockedDialect.DefinitionOfAViewModel).Return(_neverSatisfied);
            }
            using (_mocks.Playback())
            {
                Assert.That(_target.AllViewModels().Count(), Is.EqualTo(_assembly.NumberOfTypes));
                Assert.That(_target.AllViewModels().Count(), Is.EqualTo(0));
            }
        }

        [Test]
        public void AllViews_ShouldReturnAllPolicyTargetsThatAreConsideredAsViewssByTheDialect()
        {
            using (_mocks.Record())
            {
                Expect.Call(_mockedDialect.DefinitionOfAView).Return(_alwaysSatisfied);
                Expect.Call(_mockedDialect.DefinitionOfAView).Return(_neverSatisfied);
            }
            using (_mocks.Playback())
            {
                Assert.That(_target.AllViews().Count(),Is.EqualTo( _assembly.NumberOfTypes));
                Assert.That(_target.AllViews().Count(),Is.EqualTo( 0));
            }
        }
    }

    internal class SpecificationStub : Specification<IPolicyTarget>
    {
        private readonly bool _fakeAnswer;

        public SpecificationStub(bool fakeAnswer)
        {
            _fakeAnswer = fakeAnswer;
        }


        public override bool IsSatisfiedBy(IPolicyTarget obj)
        {
              return _fakeAnswer;
        }

    }

   
}
