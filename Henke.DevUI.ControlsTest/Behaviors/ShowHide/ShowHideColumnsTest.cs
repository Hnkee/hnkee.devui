﻿using System.Windows;
using System.Windows.Controls;
using Henke.DevUI.Controls.Behaviors.ShowHide;
using Henke.DevUI.TestUtilsUI;
using NUnit.Framework;

namespace Henke.DevUI.ControlsTest.Behaviors.ShowHide
{
    [TestFixture,RequiresSTA]
    public class ShowHideColumnsTest
    {
        [Test]
        public void WhenAddedToADataGrid_ItShouldNotCreateAContextMenuUntilIsLoaded()
        {
            var dataGrid = new DataGridForTest();
            dataGrid.AddBehavior();
            Assert.That(dataGrid.ContextMenu, Is.Null);
        }

        [Test]
        public void WhenAddedToADataGrid_ItShouldCreateAContextmenuwithOneCommandForeachColumn_WhenTheDataGridIsLoaded()
        {
            var dataGrid = new DataGridForTest();
            dataGrid.CreateTwoColumns();
            dataGrid.AddBehavior();

            Assert.That(dataGrid.ContextMenu, Is.Null, "Should still be null since it isnt loaded yet");

            dataGrid.Load();
            Assert.That(dataGrid.ContextMenu.Items.Count, Is.EqualTo(2));
        }

        [Test]
        public void WhenExecutingTheCommand_TheColumnShouldToggleVisibility()
        {
            var firstColumn = new DataGridTextColumn() { Visibility = Visibility.Visible };
            var secondColumn = new DataGridTextColumn() { Visibility = Visibility.Collapsed };

            var dataGrid = new DataGridForTest();
            dataGrid.AddColumn(firstColumn);
            dataGrid.AddColumn(secondColumn);

            //add the behavior....
            dataGrid.AddBehavior();
            dataGrid.Load();

            dataGrid.FirstMenuItem.Command.Execute(null);
            Assert.That(firstColumn.Visibility, Is.EqualTo(Visibility.Collapsed), "Should collapse when toggled from visible");

            dataGrid.FirstMenuItem.Command.Execute(null);
            Assert.That(firstColumn.Visibility, Is.EqualTo(Visibility.Visible), "Should toggle back to visible");

            dataGrid.SecondMenuItem.Command.Execute(null);
            Assert.That(secondColumn.Visibility, Is.EqualTo(Visibility.Visible), "Should toggle from collapsed to visible");

            dataGrid.SecondMenuItem.Command.Execute(null);
            Assert.That(secondColumn.Visibility, Is.EqualTo(Visibility.Collapsed), "Should toggle back to collapsed");
        }

        [Test]
        public void WhenExecutingTheCommand_WhenColumnIsHidden_ToggleBackToHiddenShouldSetThecolumnToHiddenNotCollapsed()
        {
            var firstColumn = new DataGridTextColumn() { Visibility = Visibility.Hidden };

            var dataGrid = new DataGridForTest();
            dataGrid.AddColumn(firstColumn);

            dataGrid.AddBehavior();
            dataGrid.Load();

            dataGrid.FirstMenuItem.Command.Execute(null);
            Assert.That(firstColumn.Visibility, Is.EqualTo(Visibility.Visible), "Should collapse when toggled from hidden");

            dataGrid.FirstMenuItem.Command.Execute(null);
            Assert.That(firstColumn.Visibility, Is.EqualTo(Visibility.Hidden), "Should toggle back to hidden, not collapsed");
        }

        [Test]
        public void AttatchingTheBeahvior_WhenContextMenuAlreadyExists_ShouldAddItemsToExistingContextMenu()
        {
            var dataGrid = new DataGridForTest();
            var contextMenu = new ContextMenu();
            contextMenu.Items.Add(new MenuItem());
            dataGrid.ContextMenu = contextMenu;
            Assert.That(dataGrid.NumberOfItemsInContextMenu, Is.EqualTo(1), "Should be one because we have a contextmenu from the beginning with one item");

            dataGrid.CreateTwoColumns();
            Assert.That(dataGrid.NumberOfItemsInContextMenu, Is.EqualTo(1), "Should still be one since we havent added the behavior");

            dataGrid.AddBehavior();
            dataGrid.Load();

            Assert.That(dataGrid.NumberOfItemsInContextMenu, Is.EqualTo(1 + 2), "Should be three, one from the beginning and one created for each column");
        }

        [Test]
        public void WhenShowHideColumnIsFalse_TheColumnShouldNotBeAddedToTheContextMenu()
        {
            var firstColumn = new DataGridTextColumn();

            ShowHideColumns.SetInclude(firstColumn, false);

            var dataGrid = new DataGridForTest();
            dataGrid.AddColumn(firstColumn);

            dataGrid.AddBehavior();
            dataGrid.Load();

            Assert.That(dataGrid.ContextMenu.Items.Count, Is.EqualTo(0));
        }

        [Test,Ignore]
        public void WhenShowHideColumnIsSetToTrue_TheColumnShouldBeAddedToTheContextMenuWithoutTheNeedToAddTheBehaviorToTheDataGrid()
        {
            var firstColumn = new DataGridTextColumn();

            ShowHideColumns.SetInclude(firstColumn, true);

            var dataGrid = new DataGridForTest();
            dataGrid.AddColumn(firstColumn);

            dataGrid.Load();

            Assert.That(dataGrid.ContextMenu.Items.Count, Is.EqualTo(1));
        }

        #region helpers

        /// <summary>
        /// Wrapper for readibility
        /// </summary>
        private class DataGridForTest
        {
            private readonly DataGrid _dataGrid;
            private DataGridTextColumn _firstColumn;
            private DataGridTextColumn _secondColumn;

            public DataGridForTest()
            {
                _dataGrid = new DataGrid();
            }

            public void CreateTwoColumns()
            {
                _firstColumn = new DataGridTextColumn();
                _secondColumn = new DataGridTextColumn();
                _dataGrid.Columns.Add(_firstColumn);
                _dataGrid.Columns.Add(_secondColumn);
            }

            public void AddColumn(DataGridColumn column)
            {
                _dataGrid.Columns.Add(column);
            }

            public Visibility FirstColumnVisibility
            {
                get { return _firstColumn.Visibility; }
                set { _firstColumn.Visibility = value; }

            }

            public Visibility SecondColumnVisibility
            {
                get { return _secondColumn.Visibility; }
                set { _secondColumn.Visibility = value; }
            }

            public MenuItem FirstMenuItem
            {
                get { return _dataGrid.ContextMenu.Items[0] as MenuItem; }
            }

            public MenuItem SecondMenuItem
            {
                get { return _dataGrid.ContextMenu.Items[1] as MenuItem; }
            }

            public ContextMenu ContextMenu
            {
                get { return _dataGrid.ContextMenu; }
                set { _dataGrid.ContextMenu = value; }
            }

            public void AddBehavior(ShowHideColumns showHideColumns)
            {
                System.Windows.Interactivity.Interaction.GetBehaviors(_dataGrid).Add(showHideColumns);
            }

            public void AddBehavior()
            {
                AddBehavior(new ShowHideColumns());
            }

            public int NumberOfItemsInContextMenu
            {
                get { return ContextMenu.Items.Count; }
            }

            public void Load()
            {
                new XamlUtils().RaiseLoadedEvent(_dataGrid);
            }
        }
        #endregion
    }
}
