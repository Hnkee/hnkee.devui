﻿using System.Windows;
using System.Windows.Controls;
using Henke.DevUI.Controls.Behaviors.ShowHide;
using NUnit.Framework;

namespace Henke.DevUI.ControlsTest.Behaviors.ShowHide
{
    [TestFixture, RequiresSTA]
    public class ShowHideOnCommandBehaviorTest
    {

        [Test]
        public void WhenAttatched_AContextMenuShouldBeCreatedForTheTarget()
        {
            Button button = new Button();
            AttatchTarget_To(button);
            Assert.That(button.ContextMenu.Items.Count > 0);
        }

        [Test]
        public void OnAttatched_WhenTheTargetAlreadyHasAContextMenu_TheShowHideCommandShouldBeAddedToTheContextMenu()
        {
            var button = CreateAButtonWithAContextMenuThatContainsTwoItems();
            int numberOfMenuItemsInContextMenuBeforeAddingTheBehavior = button.ContextMenu.Items.Count;
            AttatchTarget_To(button);
            Assert.That(button.ContextMenu.Items.Count, Is.EqualTo(numberOfMenuItemsInContextMenuBeforeAddingTheBehavior + 1));
        }

        [Test]
        public void TheShowHideCommandAddedToTheContextMenu_WhenExecuted_ShouldToggleVisibilityOfTheTargetObject()
        {
            Button button = new Button();
            button.Visibility = Visibility.Visible;

            AttatchTarget_To(button);
            var firstMenuItem = button.ContextMenu.Items[0] as MenuItem;
            Assert.That(firstMenuItem, Is.Not.Null);

            firstMenuItem.Command.Execute(null);
            Assert.That(button.Visibility, Is.EqualTo(Visibility.Collapsed), "When executed the object should toggle the visibility to collapsed");

            firstMenuItem.Command.Execute(null);
            Assert.That(button.Visibility, Is.EqualTo(Visibility.Visible), "When executed the object should toggle the visibility back to visible (normally this will not be possible)");
        }

        [Test]
        public void TheMenuItem_ShouldHaveItsHeaderSetFromTheBehavior()
        {
            Button button = new Button();
            button.Visibility = Visibility.Visible;

            AttatchTarget_To(button, "header");
            var firstMenuItem = button.ContextMenu.Items[0] as MenuItem;
            Assert.That(firstMenuItem.Header, Is.EqualTo("header"));
        }

        #region helpers
        private static void AttatchTarget_To(FrameworkElement targetElement)
        {
            System.Windows.Interactivity.Interaction.GetBehaviors(targetElement).Add(new ShowHideOnCommandBehavior());
        }

        private static void AttatchTarget_To(FrameworkElement targetElement, object header)
        {
            System.Windows.Interactivity.Interaction.GetBehaviors(targetElement).Add(new ShowHideOnCommandBehavior() { Header = header });
        }

        private static Button CreateAButtonWithAContextMenuThatContainsTwoItems()
        {
            var contextmenu = new ContextMenu();
            contextmenu.Items.Add(new MenuItem());
            contextmenu.Items.Add(new MenuItem());
            return new Button() { ContextMenu = contextmenu };
        }
        #endregion
    }
}