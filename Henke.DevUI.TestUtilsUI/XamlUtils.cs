﻿using System.Reflection;
using System.Windows;

namespace Henke.DevUI.TestUtilsUI
{
    public class XamlUtils
    {
        /// <summary>
        /// Raises the Loaded event
        /// </summary>
        /// <param name="element"></param>
        /// <remarks>
        /// A lot of times we need to hook up and do stuff when the frameworkElement is loaded (esp. when testing behaviors and such). 
        /// </remarks>
        public void RaiseLoadedEvent(FrameworkElement element)
        {
            MethodInfo eventMethod = typeof(FrameworkElement).GetMethod("OnLoaded", BindingFlags.Instance | BindingFlags.NonPublic);
            RoutedEventArgs args = new RoutedEventArgs(FrameworkElement.LoadedEvent);
            eventMethod.Invoke(element, new object[] { args });
        } 
    }
}
