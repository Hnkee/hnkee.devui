﻿namespace Henke.DevUI.MVVMUtils.Messaging.Helpers
{
    public interface IExecuteWithObject
    {
        void ExecuteWithObject(object parameter);
    }
}
