﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Henke.DevUI.MVVMUtils
{
    public class ViewModelBase :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyname)
        {
            if(PropertyChanged!=null)PropertyChanged(this,new PropertyChangedEventArgs(propertyname));
        }

        protected void NotifyPropertyChanged<TPropertyType>(Expression<Func<TPropertyType>> expression)   
        {   
            NotifyPropertyChanged( GetPropertyNameFromExpression(expression));
        }   

        private static string GetPropertyNameFromExpression<TPropertyType>(Expression<Func<TPropertyType>> expression)   
        {   
            var memberExpression = expression.Body as MemberExpression;   
            if (memberExpression == null)   
                throw new ArgumentException(String.Format(   
                    "'{0}' is not a member expression", expression.Body));   
            return memberExpression.Member.Name;   
        }  

    }
}
