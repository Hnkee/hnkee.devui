﻿using Henke.DevUI.TestUtils.Common;
using Henke.DevUI.Utils.Specifications;
using NUnit.Framework;

namespace Henke.DevUI.TestUtilsTest.Common
{
    [TestFixture]
    public class SpecificationToConstraintTest
    {
        private string _parameter;
        private SpecificationStub _alwaysSatisfied;
        private SpecificationStub _neverSatisfied;

        [SetUp]
        public void Setup()
        {
            _parameter = "some parameter";
            _alwaysSatisfied = new SpecificationStub(true);
            _neverSatisfied = new SpecificationStub(false);
        }

        [Test]
        public void Matches_WhenSpecificationIsSatisfied_ShouldReturnTrue()
        {
            var constraint = _alwaysSatisfied.ToConstraint();
            Assert.That(constraint.Matches(_parameter));
            Assert.That(_alwaysSatisfied.WasCalledwith(_parameter));
        }

        [Test]
        public void Matches_WhenSpecificationIsNotSatisfied_ShouldReturnFalse()
        {
            var constraint = _neverSatisfied.ToConstraint();
            Assert.That(constraint.Matches(_parameter),Is.False);
            Assert.That(_neverSatisfied.WasCalledwith(_parameter));
        }

        [Test,ExpectedException(typeof(AssertionException))]
        public void AssertionSyntaxSample()
        {
            const string notAnAdress = "some string that does not satisfy the specification";
            var isValidAdress = _neverSatisfied.ToConstraint();

            Assert.That(notAnAdress,isValidAdress);
        }

        private class SpecificationStub : Specification<string>
        {
            private readonly bool _fakeResult;
            private string _calledWithParameter;

            public SpecificationStub(bool fakeResult)
            {
                _fakeResult = fakeResult;
            }

            public override bool IsSatisfiedBy(string obj)
            {
                _calledWithParameter = obj;
                return _fakeResult;
            }

            public bool WasCalledwith(string parameter)
            {
                return parameter.Equals(_calledWithParameter);
            }
        }
    }
}
