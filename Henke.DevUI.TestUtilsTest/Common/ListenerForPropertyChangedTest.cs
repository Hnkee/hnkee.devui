﻿using System;
using Henke.DevUI.MVVMUtils;
using Henke.DevUI.TestUtils.Common;
using NUnit.Framework;

namespace Henke.DevUI.TestUtilsTest.Common
{
    [TestFixture]
    public class ListenerForPropertyChangedTest
    {
        private NotifyPropertyChangedStub _target;
        private string _property1;
        private string _property2;

        [SetUp]
        public void Setup()
        {
            _target = new NotifyPropertyChangedStub();
            _property1 = "MyProperty";
            _property2 = "MyOtherProperty";
        }

        [Test]
        public void ListenTo_ShouldFail_WhenPropertyChangedIsntFired()
        {
            try
            {
                using (new ListenerForPropertyChanged().ListenTo(_target).For(_property1))
                {
                    //Not doing anything should fail this test since the propertychanged isnt fired
                }
            }
            catch (AssertionException e)
            {
                VerifyThatFailedForTheRightReason(e, _property1);
            }
        }

        [Test]
        public void ListenTo_ShouldFail_WhenPropertyChangedIsFiredWithAnotherProperty()
        {
            try
            {
                using (new ListenerForPropertyChanged().ListenTo(_target).For(_property2))
                {
                    //Changing another property (not the property we are listening for) should fail the test
                    _target.MyProperty = "new value";
                }
            }
            catch (AssertionException e)
            {
                VerifyThatFailedForTheRightReason(e, _property2);
            }
        }

        [Test]
        public void And_ShouldFail_IfNotAllpropertiesAreFired()
        {
            try
            {
                using (new ListenerForPropertyChanged().ListenTo(_target).For("MyOtherProperty").And(_property1))
                {
                    //Not changing all of the properties we are listeneing for should fail the test
                    _target.MyProperty = "new value";
                }
            }
            catch (AssertionException e)
            {
                VerifyThatFailedForTheRightReason(e, _property2);
            }
        }

        [Test]
        public void ListenTo_UsingMemberExpressions()
        {
            try
            {
                using (new ListenerForPropertyChanged().ListenTo(_target).For(() => _target.MyOtherProperty))
                {
                    //Changing another property (not the property we are listening for) should fail the test
                    _target.MyProperty = "new value";
                }
            }
            catch (AssertionException e)
            {
                VerifyThatFailedForTheRightReason(e, _property2);
            }
        }

        [Test, ExpectedException(typeof(ArgumentNullException))]
        public void ListenTo_WhenTargetIsNull_ShouldThrowArgumentexception()
        {
            var listener = new ListenerForPropertyChanged().ListenTo(null);
            Assert.That(listener, Is.Null, "This will not be reached (if test works properly)");
        }

        private static void VerifyThatFailedForTheRightReason(AssertionException assertionException, string partOfMessage)
        {
            Assert.That(assertionException.Message.Contains(partOfMessage));
        }

        /// <summary>
        /// Simulates a typical implementation of a viewmodel
        /// </summary>
        private class NotifyPropertyChangedStub : ViewModelBase
        {
            private string _myProperty;
            private string _myOtherProperty;

            public string MyProperty
            {
                get { return _myProperty; }
                set
                {
                    _myProperty = value;
                    NotifyPropertyChanged(() => MyProperty);
                }
            }


            public string MyOtherProperty
            {
                get { return _myOtherProperty; }
                set
                {
                    _myOtherProperty = value;
                    NotifyPropertyChanged(() => MyOtherProperty);
                }
            }
        }

    }
}
