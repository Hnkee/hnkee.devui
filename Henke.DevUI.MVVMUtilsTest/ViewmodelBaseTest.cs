﻿using Henke.DevUI.MVVMUtils;
using NUnit.Framework;


namespace Henke.DevUI.MVVMUtilsTest
{
    [TestFixture]
    public class ViewModelBaseTest
    {
        private ViewModelStub _target;

        [SetUp]
        public void Setup()
        {
            _target = new ViewModelStub();
        }

        [Test]
        public void NotifyPropertyChanged_WhenCalledWithStringParameter_ShouldInvokePropertyChangedEvent()
        {
            string propertyNameArg = string.Empty;
            object caller = string.Empty;
            _target.PropertyChanged += (sender, args) =>
                                           {
                                               propertyNameArg = args.PropertyName;
                                               caller = sender;
                                           };
            _target.CallPropertyChangedWithString();
            
            Assert.That(propertyNameArg,Is.EqualTo(ViewModelStub.PropertyName));
            Assert.That(caller,Is.SameAs(_target));
        }

        [Test]
        public void NotifyPropertyChanged_WhenCalledWithMemberExpression_ShouldInvokePropertyChangedEvent()
        {
            string propertyNameArg = string.Empty;
            object caller = string.Empty;
            _target.PropertyChanged += (sender, args) =>
            {
                propertyNameArg = args.PropertyName;
                caller = sender;
            };
            _target.CallPropertyChangedWithMemberExpression();

            Assert.That(propertyNameArg, Is.EqualTo(ViewModelStub.PropertyName));
            Assert.That(caller, Is.SameAs(_target));
        }

        private class ViewModelStub : ViewModelBase
        {
            public static string PropertyName
            {
                get { return "SomeProperty"; }
            }

            public void CallPropertyChangedWithString()
            {
                NotifyPropertyChanged(PropertyName);
            }

            public void CallPropertyChangedWithMemberExpression()
            {
                NotifyPropertyChanged(()=>SomeProperty);
            }

            private string SomeProperty { get; set; }
        }
    }
}
