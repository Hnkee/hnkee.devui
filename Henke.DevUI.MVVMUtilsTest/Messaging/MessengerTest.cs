﻿using System.Collections.Generic;
using Henke.DevUI.MVVMUtils.Messaging;
using NUnit.Framework;

namespace Henke.DevUI.MVVMUtilsTest.Messaging
{
    [TestFixture]
    public class MessengerTest
    {
        private IList<SampleMessage> _recievedMessages;
        private IMessenger _target;

        [SetUp]
        public void Setup()
        {
            _recievedMessages = new List<SampleMessage>();
            _target = new Messenger();
            Register();
        }

        [Test]
        public void VerifyGenericMessage()
        {
            var message = new SampleMessage { SampleProperty = "hello" };
            Assert.AreEqual(0, _recievedMessages.Count);
            _target.Send(message);
            Assert.AreEqual(message, _recievedMessages[0]);
        }

        [Test]
        public void VerifyDerivedMessages()
        {
            var message = new DerivedSampleMessage { SampleProperty = "hello" };
            SampleMessage message2 = new DerivedSampleMessage { SampleProperty = "hello" };

            _target.Send(message);
            Assert.AreEqual(0, _recievedMessages.Count);

            _target.Send(message2);
            Assert.AreEqual(1, _recievedMessages.Count);
        }

        private void Register()
        {
            _target.Register<SampleMessage>(this, s => _recievedMessages.Add(s));
        }

    }

    internal class SampleMessage
    {
        public string SampleProperty { get; set; }
    }

    internal class DerivedSampleMessage : SampleMessage
    {

    }
}
